#include <stm32f37x.h>
#include <stdbool.h>
#include <stdio.h>

#define TimerTick				SystemCoreClock/10-1
#define ITM_Port8(n)    (*((volatile unsigned char *)(0xE0000000+4*n)))
#define ITM_Port16(n)   (*((volatile unsigned short*)(0xE0000000+4*n)))
#define ITM_Port32(n)   (*((volatile unsigned long *)(0xE0000000+4*n)))
#define DEMCR           (*((volatile unsigned long *)(0xE000EDFC)))
#define TRCENA          0x01000000

GPIO_InitTypeDef GPIO_InitStructure;
volatile bool state = true;
volatile unsigned int div = 0;


//------------------------------------------------------------


// ��������� ���� ��� ��������� ��� ����, ����� ���������� pritnf()
// �� ����� SWO ��������� JLink - ����� ������� � ��� ����������
// ����������� COM-����, ���������� ������ �� ��������
struct __FILE { int handle; };
FILE __stdout;
FILE __stdin;

int fputc(int ch, FILE *f) {
  if (DEMCR & TRCENA) {
    while (ITM_Port32(0) == 0);
    ITM_Port8(0) = ch;
  }
  return(ch);
}

// ���������� ���������� �� ���������� �������
// � �������� �������� �� ��� ������ ������� ������� delay_ms
void SysTick_Handler(void)
{
	if (div < 9) // ����� ������� �� 10
	{
		div++;
		return;
	}
	else div = 0;
	
	if (state) GPIO_SetBits(GPIOB, GPIO_Pin_9);
	else       GPIO_ResetBits(GPIOB, GPIO_Pin_9);
	state = !state;
	
	printf("state = %x\r\n",state);
}	

//------------------------------------------------------------

int main(void)
{
	// ������������� ����������� PB8 � PB9
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_8;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	// ������������� ����� PA8, � �������� ��������� ������������ 72MHz
	RCC_MCOConfig(RCC_MCOSource_SYSCLK);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	GPIO_PinAFConfig(GPIOA, GPIO_Pin_8, GPIO_AF_0);

	// ������������� ���������� ������� (������ �������� � ������ UP �� TimerTick, ����� ������������ � �������� ����������)
	SysTick->LOAD = TimerTick;
	SysTick->VAL  = TimerTick;
	SysTick->CTRL = SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_TICKINT_Msk | SysTick_CTRL_ENABLE_Msk;
	

	for(;;)
	{
		__ASM("NOP");
	}
}

//------------------------------------------------------------

